﻿using UnityEngine;
using System.Collections;

public class DeathBlock : MonoBehaviour {

	// Use this for initialization

	public static float speed = 1;

	void Start () {
		Respawn();
	}
	public void Respawn()
	{
		transform.eulerAngles = new Vector3(90,transform.eulerAngles.y+90*Mathf.Floor(Random.Range(0,4)),0);
		transform.position = UIScaler.FindSpaceInBounds(true, true);
		transform.position = new Vector3(transform.position.z, .1f,transform.position.y);
		StartCoroutine(Crush ());
	}
	// Update is called once per frame
	void Update () {
		transform.Translate(transform.forward*Time.deltaTime*speed);
	}

	
	void OnCollisionEnter(Collision collision) {
		if(!gameObject.CompareTag("Player"))
		transform.eulerAngles = new Vector3(90,transform.eulerAngles.y+90,0);
	}
	void OnCollisionStay(Collision collision) {
		if(!gameObject.CompareTag("Player"))
		transform.eulerAngles = new Vector3(90,transform.eulerAngles.y+90,0);
	}
	
	IEnumerator Crush(bool reverse = true)
	{
		float modifier = 1;
		
		float progress = 0;

		if(reverse)
		{
			modifier = -1.3f;
			progress = .999f;
		}
		
		while(progress<1&&progress>=0)
		{
			progress+=Time.deltaTime*1.9f*modifier;
			Mathf.Max(progress, 0);
			transform.localScale = new Vector3(.57f,.57f*(1-progress),.57f);
			if(progress ==0)
				progress--;
			yield return null;
		}
	}
}
