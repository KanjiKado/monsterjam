﻿using UnityEngine;
using System.Collections;

public class EnemyManager : MonoBehaviour {

	public static int enemiesKilled = 0;
	public static int enemiesAlive = 0;
	int currentLevel = 1;
	bool repairedWalls = false;
	PlayerController player;

	// Use this for initialization
	void Start () {
		Reset ();
		player = FindObjectOfType<PlayerController>();
		SpawnLevel();
	}
	public static void Reset()
	{
		Squishy.speed=1;
		enemiesKilled = 0;
		enemiesAlive = 0;
		DeathBlock.speed=-2;

	}
	
	// Update is called once per frame
	void Update () {
		if(enemiesAlive == 0)
		{
			if(!repairedWalls)
			{
				RepairWalls();
				currentLevel++;
				FindObjectOfType<UIManager>().DisplayLevel(currentLevel);
				DeathBlock.speed+=.8f;
				Squishy.speed++;
				repairedWalls = true;
			}
			if(player.isMoving()){
				SpawnLevel();
			}
		}
	}

	void SpawnLevel()
	{
		UIScaler.ClearAvoidance();
		for(int i  =0; i<SpawnCountFor(currentLevel); i++)
		{
			Squishy s = PoolManager.GetEnemy();
			s.Respawn();
			enemiesAlive++;
		}
		if(currentLevel >=3 && currentLevel%3 ==0){
			DeathBlock d = PoolManager.GetSaw();
			d.Respawn();
		}
		
		repairedWalls = false;
	}
	public static void RepairWalls(bool fully = false)
	{
		if(fully)
			foreach(Wall w in FindObjectsOfType<Wall>())
				w.HealDamage();
		else{
			int tries = 0;
			Wall[] ws = FindObjectsOfType<Wall>();
			bool healed;
			int slot = Mathf.RoundToInt(Mathf.Floor(Random.Range(0,4)));
			do{
				slot = (slot+tries)%4;
				tries++;
				healed = ws[slot].FullHealDamage();
			}while (!healed&&tries<ws.Length);
			if(!healed){
				tries = 0;
				slot = Mathf.RoundToInt(Mathf.Floor(Random.Range(0,4)));
				do{
					slot = (slot+tries)%4;
					tries++;
				}while (!ws[slot].HealDamage()&&tries<ws.Length);
			}
		}

	}

	public static void RepairAllWalls()
	{
		int tries = 0;
		Wall[] ws = FindObjectsOfType<Wall>();
		bool healed;
		int slot = Mathf.RoundToInt(Mathf.Floor(Random.Range(0,4)));
		healed = ws[slot].FullHealDamage(true);
	}
	int SpawnCountFor(int level)
	{
		if(level>5)
			return 4+level/2;

		if(level>3)
			return 6;

		if(level>1)
			return 5;

		return 4;
	}
}
