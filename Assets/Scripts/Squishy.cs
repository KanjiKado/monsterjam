﻿using UnityEngine;
using System.Collections;

public class Squishy : MonoBehaviour {

	Vector3 idealTarget;
	Vector3 currentTarget;
	float stunnedPeriod;
	public static float speed = 1f;
	float wobbleAmount;//this is a cirlce
	float acceleraitonBonus = .1f;
	Collider collider;
	bool canMove = true;
	float orientation = 0;
	float addToOrientation = 0;
	float defaultDirection;
	float wobbleRate;

	void Start()
	{
		Respawn ();
	}

	public void Respawn(int dof = 0)//yeah whatever come at me programming police
	{
		if(collider==null)
		{
			collider = transform.GetComponent<Collider>();
		}
		transform.parent.parent = null;
		wobbleRate = Random.Range(.5f,1.2f);
		wobbleAmount = Random.Range(0, Mathf.PI*2);
		currentTarget = -transform.forward;
		StartCoroutine(
			Crush(true));

		transform.parent.position =UIScaler.FindSpaceInBounds(true);//avoidPlayer = true;
		int randomDir;
		if(dof<2){
			//if > player, go down. if < player, go up. if == player, go random
			randomDir = 1;
			if(dof>=1)
				
				randomDir = 2;
				//do the same for y
		}else
			
			randomDir = 3;

				//go rando!
		defaultDirection = transform.parent.eulerAngles.y;
		//BeginNod();
		RecalculateIdeal();
	}

	// Update is called once per frame
	void Update () {
		if(addToOrientation>0){
			float temp = Time.deltaTime*speed*6;
			addToOrientation -=temp;
			orientation+=temp;
		}
		Wobble();
		if(canMove)
		{
			acceleraitonBonus+=(.01f+acceleraitonBonus*1.1f)*Time.deltaTime;
			acceleraitonBonus = Mathf.Min(acceleraitonBonus , .25f);
			transform.Translate(-transform.forward*.25f*speed*Time.deltaTime);
		} else {
			acceleraitonBonus-=.25f*Time.deltaTime;
		}
		//the acceleration no matter what
		
		acceleraitonBonus = Mathf.Max(0, acceleraitonBonus);
		transform.Translate(currentTarget*acceleraitonBonus*Time.deltaTime);
	}

	void FrightenedIdeal()
	{

	}

	void InstantIdeal()
	{
		currentTarget = idealTarget;
	}

	
	void Shocked()
	{
		//sometimes we get startled before we can go to our ideal path.
	}

	//this is called more frequently 
	void RecalculateIdeal()
	{
		addToOrientation = Random.Range(100,200)*Mathf.Floor(Random.Range(0,2)*2)-1;
		if(Mathf.Ceil(Random.Range(0,2))==1){
			orientation+= 14*(Mathf.Clamp(Mathf.Sin(wobbleAmount%(Mathf.PI*2))*1.2f,-1,1));
			wobbleAmount = (Mathf.Ceil(Random.Range(0,2))==.04f ? .08f : Mathf.PI);
		}
	}

	void Wobble()
	{
		wobbleAmount += (speed*2*Time.deltaTime+(Mathf.Sin(wobbleAmount%Mathf.PI*2)/3)*Time.deltaTime)*wobbleRate;
		//if(!colliding with somehting...)
		float sineVal = (Mathf.Clamp(Mathf.Sin(wobbleAmount%(Mathf.PI*2)),-1,1));//*1.2f
		transform.parent.eulerAngles = new Vector3(90,defaultDirection,orientation+14*sineVal);

		if(Mathf.Abs(sineVal)==1){
			canMove = false;
		}else{
			if(canMove == false)
			RecalculateIdeal();
			canMove = true;
		}
		//wobble as we walk
	}

	void BecomeAlert()
	{
		//will now activley avoid player
	}

	//one way that AI improves is curving tightness/moving speed/alert distance

	public void Kill()
	{
		EnemyManager.enemiesAlive--;
		EnemyManager.enemiesKilled++;
			PoolManager.SpawnParticles(transform.position);
		//depending on direciton crush 
		StartCoroutine(Crush ());
	}
	IEnumerator Crush(bool reverse = false)
	{
		float modifier = 1;
		
		float progress = 0;
		
		collider.enabled = false;
		if(reverse)
		{
			modifier = -1.3f;
			progress = .999f;
				collider.enabled = true;
		}

		while(progress<1&&progress>=0)
		{
			progress+=Time.deltaTime*1.9f*modifier;
			Mathf.Max(progress, 0);
			transform.localScale = new Vector3(.25f,.25f*(1-progress),.25f);
			if(progress ==0)
				progress--;
			yield return null;
		}
		if(!reverse)
		Despawn();
	}

	void BeginNod()
	{
		StartCoroutine(Nod());
	}

	IEnumerator Nod()
	{

		float nodProgress = 0;
		while(nodProgress<1)
		{
			nodProgress+=Time.deltaTime/.66f;
			transform.parent.eulerAngles = new Vector3(transform.parent.eulerAngles.x, .24f*Mathf.Sin(nodProgress*Mathf.PI), transform.parent.eulerAngles.z);
			yield return null;
		}

	}
	void Despawn () 
	{
		gameObject.SetActive(false);
		transform.parent.parent = PoolManager.GetPoolTransform();
	}
}
