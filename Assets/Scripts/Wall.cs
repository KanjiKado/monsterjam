﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class Wall : MonoBehaviour {



	private int lives = 2;
	private int damage = 0;
	Collider collider;
	bool isBreakable = false;
	private GameController gameController;

	void Start()
	{
		gameController = FindObjectOfType<GameController>();
		collider = GetComponent<Collider>();
	}
	// Something hit us!
	public void Damage(int amt)
	{
		
		if(amt>1)
		{
			SpawnOuch();
			
			if(damage !=lives){
				damage = lives ;
				SpawnFrag();
			}
		}
		if(damage<lives){
		damage+= amt;
			SpawnFrag();
		}

	}
	void SpawnFrag()
	{
		GameObject g = GameObject.Instantiate(gameController.GetFragReference());
		g.transform.rotation = transform.rotation;
		g.transform.position = transform.position;
		g.transform.position = new Vector3(g.transform.position.x,.25f, g.transform.position.z);
		g.GetComponent<Renderer>().material.color =GetComponent<Renderer>().material.color;
		GetComponent<Renderer>().material.color = Color.Lerp(gameController.GetBackgroundColor(), gameController.GetForegroundColor(), 1-damage/(float)(lives));
		
		if(damage>=lives)
			WarningFlash();
	}
	void WarningFlash()
	{
		
		StopCoroutine("Warn");
		this.StartCoroutine(Warmn());
	}
	void SpawnOuch()
	{
		GameObject g = GameObject.Instantiate(gameController.GetFragReference());
		g.transform.rotation = transform.rotation;
		g.transform.position = transform.position;
		g.transform.position = new Vector3(g.transform.position.x,.25f, g.transform.position.z);
		g.GetComponent<FlyingWall>().DisableMotion();
		gameController.HandleSlam();
	}
	public IEnumerator Warmn(bool die = true)
	{
		float time = 1;
		Color cached;
			if(die)
				cached = Color.Lerp(gameController.GetBackgroundColor(), gameController.GetForegroundColor(), 1/(float)(lives));
		else
			cached = Color.white;
		Color target;
		if(die)
			target = gameController.GetBackgroundColor();
		else
			target = gameController.GetForegroundColor();
		while(time>0){
			time-=Time.deltaTime;
			GetComponent<Renderer>().material.color = Color.Lerp(target, cached, (1-Mathf.Cos(Mathf.PI*time)*3)%1);
			if(die){
			if(time<.5f)
				collider.enabled = false;
			} else
				
				collider.enabled = true;
			yield return null;
		}
		GetComponent<Renderer>().material.color = target;
	}

	public bool HealDamage(bool forceHeal =false)
	{
		if(!forceHeal&&damage==0)
			return false;
		//damage-=amt;
			damage = 0;
		Debug.Log("Healll");
		StopCoroutine("Warn");
		if(collider!=null)
			collider.enabled = true;
		this.StartCoroutine(Warmn(false));
		return true;

	}
	public bool FullHealDamage(bool forceHeal = false)
	{
		if(forceHeal || damage>=lives)
			return HealDamage(forceHeal);
		return false;
	}

}
