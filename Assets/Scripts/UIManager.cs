﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
	float timeValue = 15f;
	public Text text;
	PlayerController player;
	public Text click;
	public Text combo;
	public Text jumps;
	public Text level;
	public Text boostNotify;

	
	public Text heading;
	public Text killCount;
	public Text deathReason;
	public Text again;
	public Text best;
	public Text credits;
	float gameOverTime = 0;


	public static bool started = false;
	//fl
	bool begun = false;
	float promptDelay = 2;
	float comboTimer = 0;
	float levelDisplay = 3.2f;
	float baseLevelPos =0;
	float boostVal = 0;
	float wallTimer = 0;
	float wallTimer2 = 0;
	Color baseTimerColor;
	Color againColor;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController>();
		baseLevelPos = level.transform.position.y;
		level.transform.position = new Vector3(level.transform.position.x, baseLevelPos+200,level.transform.position.z);
		baseTimerColor = text.color;
	}
	
	// Update is called once per frame
	void Update () {

		if(!player.isDead()){
		if(!started){
				promptDelay-=Time.deltaTime/3;
				begun = true;
			if(promptDelay<1)
			{
				promptDelay = (promptDelay+1)%1;
				click.color = Color.Lerp(new Color(255,255,255,0),Color.white,Mathf.Sin(promptDelay*Mathf.PI)/1.8f);
			}
			
		}
		else{
				if(begun!=true){
					FindObjectOfType<GameController>().ShakeCamera(Random.Range(0,360),15f);
					begun = true;
				}
			if(levelDisplay<2.5f){
				
				if(wallTimer2 >0)
				{
					wallTimer2-=Time.deltaTime;
					jumps.color = Color.Lerp(new Color(255,255,255,0),Color.white,wallTimer2/2);
					}else
					jumps.color = new Color(255,255,255,0);
				levelDisplay+=Time.deltaTime*1.4f;
				level.transform.position = new Vector3(level.transform.position.x, Mathf.Lerp(baseLevelPos+200, baseLevelPos,levelDisplay*2),level.transform.position.z);
			} else{

				level.transform.position = new Vector3(level.transform.position.x, baseLevelPos+200,level.transform.position.z);
				}

			if(!player.isDead()){
			click.color = Color.clear;
			timeValue -= Time.deltaTime*1.1f;
			timeValue = Mathf.Clamp(timeValue,0, 40);
			if(timeValue<=.4f)
			{
					text.color = Color.Lerp(new Color(255,255,255,0),Color.white,.8f);
					player.deathReason = "ran out of time";
					player.Die (true);
			}
			text.text = timeValue.ToString("F0");
			}
			if(comboTimer>0)
			{
				comboTimer-=Time.deltaTime/2;
				combo.enabled = true;
			} else
				combo.enabled = false;
			
			if(boostVal>0)
			{
				boostVal-=Time.deltaTime;
				text.color = Color.Lerp(baseTimerColor, Color.white, boostVal*2);
			}
			if(wallTimer >0)
			{
				wallTimer-=Time.deltaTime;
				boostNotify.color = Color.Lerp(new Color(255,255,255,0),Color.white,wallTimer/2);
			} else
				boostNotify.color = new Color(255,255,255,0);


		}
		}else
			GameOverUI();
	}

	public void GameOverUI(){

		if(Input.GetMouseButtonDown(0))
			if(gameOverTime<1.3f)
				gameOverTime = 1.3f;
			else
			Application.LoadLevel(Application.loadedLevel);

		gameOverTime+=Time.deltaTime;
		if(gameOverTime>.15f)
			killCount.enabled = true;
		if(gameOverTime>.56f)
			deathReason.enabled = true;

		if(gameOverTime>1.3f)
		{
			again.enabled = true;
			best.enabled = true;
			credits.enabled = true;
			again.color = Color.Lerp(Color.white, againColor, Mathf.Sin(((gameOverTime*2)%1)*Mathf.PI));
		}
	}

	public void DisplayDeath()
	{
		//disable game UI
		text.enabled = false;
		click.enabled = false;
		combo.enabled = false;
		jumps.enabled = false;
		level.enabled = false;
		boostNotify.enabled = false;

		againColor = again.color;

		//enable death UI as it's needed
		heading.enabled = true;
		killCount.text = ""+EnemyManager.enemiesKilled;
		deathReason.text = "before you "+player.deathReason+"\n";
		//again;
		if(EnemyManager.enemiesKilled>PlayerPrefs.GetInt("Score",0))
			PlayerPrefs.SetInt("Score",EnemyManager.enemiesKilled);
		best.text = "best: "+PlayerPrefs.GetInt("Score");
		//credits;
		gameOverTime = 0;

	}
	
	public void DisplayCombo(int amt) {
		combo.text = amt + "x combo!";
		comboTimer=.85f;
		EnemyManager.RepairWalls();
		wallTimer = 1.6f;
	}
	
	public void DisplayLevel(int amt) {
		if(amt>1){
			level.text = "Level "+amt;
			levelDisplay = 0;
			timeValue = Mathf.Ceil(timeValue)+1.2f;
			boostVal = .6f; 
			wallTimer2 = 1.6f;
		}
	}
}
