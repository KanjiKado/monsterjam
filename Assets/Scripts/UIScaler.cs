﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Camera))]
public class UIScaler : MonoBehaviour {

	// Use this for initialization
	Camera c;
	public GameObject nearSide;
	public GameObject farSide;
	public GameObject nearSideOverlay;
	public GameObject farSideOverlay;

	private static Vector3 topRect;
	private static Vector3 bottomRect;
	private static Vector3 leftRect;
	private static Vector3 rightRect;

	public static List<Vector3> locationsToAvoid = new List<Vector3>();

	[HideInInspector]
	public static float xLimit;
	
	[HideInInspector]
	public static float yLimit;

	public static float xBound;
	public static float zBoundTop;
	public static float zBoundBot;

	public static Transform cachedPlayer;

		
		[HideInInspector]
		public
	void Start () {
		c = GetComponent<Camera>();
		RecomputeBounds();
		cachedPlayer =  GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//call this on init/screen resize
	void RecomputeBounds () {

		//compensate for one side being bigger (heh)

		GameObject parent = new GameObject();

		Transform parentTransform = parent.transform;

		float fudge = (Screen.height>Screen.width ? +.005f : -.005f);

		//screenspace to worldspace

		float maxBound = .985f;
		float midBound = .5f;
		float minBound = .015f;

		xLimit = minBound+fudge;
		yLimit = maxBound-fudge;

		//the object's rotation and prefab offset must already be set, that's why we got NS and FS

		//right side
		GameObject g1 = GameObject.Instantiate(farSide);
		g1.transform.position = c.ViewportToWorldPoint(new Vector3(maxBound-fudge,midBound,0));
		g1.transform.Translate(Vector2.down);
		g1.transform.parent = parentTransform;

		xBound = g1.transform.position.x+8;
		rightRect = g1.transform.position;
		//first one
		GameObject g2 = GameObject.Instantiate(nearSide);
		g2.transform.position = c.ViewportToWorldPoint(new Vector3(minBound+fudge,midBound,0));
		g2.transform.Translate(Vector2.down);
		g2.transform.parent = parentTransform;

		leftRect = g2.transform.position;

		//first one
		GameObject g3 = GameObject.Instantiate(nearSide);
		g3.transform.position = c.ViewportToWorldPoint(new Vector3(midBound,maxBound+fudge,0));
		g3.transform.eulerAngles = (new Vector3(0,90,0));
		g3.transform.Translate(Vector2.down);
		g3.transform.parent = parentTransform;

		zBoundTop = g3.transform.position.z+8;
		topRect = g3.transform.position;

				
		//first one
		GameObject g4 = GameObject.Instantiate(farSide);
		g4.transform.position = c.ViewportToWorldPoint(new Vector3(midBound,minBound-fudge,0));
		g4.transform.eulerAngles = (new Vector3(0,90,0));
		g4.transform.Translate(Vector2.down);
		g4.transform.parent = parentTransform;
		
		zBoundBot = g4.transform.position.z-8;
		
		bottomRect = g4.transform.position;

		//assign normal material

		//now we cover the corners... dirty black magic

		
		//right side
		GameObject g1b = GameObject.Instantiate(farSideOverlay);
		g1b.transform.position = c.ViewportToWorldPoint(new Vector3(maxBound-fudge,midBound,0));
		g1b.transform.Translate(Vector2.down);
		g1b.transform.parent = parentTransform;
		
		
		//first one
		GameObject g2b = GameObject.Instantiate(nearSideOverlay);
		g2b.transform.position = c.ViewportToWorldPoint(new Vector3(minBound+fudge,midBound,0));
		g2b.transform.Translate(Vector2.down);
		g2b.transform.parent = parentTransform;
		
		//first one
		GameObject g3b = GameObject.Instantiate(nearSideOverlay);
		g3b.transform.position = c.ViewportToWorldPoint(new Vector3(midBound,maxBound+fudge,0));
		g3b.transform.eulerAngles = (new Vector3(0,90,0));
		g3b.transform.Translate(Vector2.down);
		g3b.transform.parent = parentTransform;
		
		//first one
		GameObject g4b= GameObject.Instantiate(farSideOverlay);
		g4b.transform.position = c.ViewportToWorldPoint(new Vector3(midBound,minBound-fudge,0));
		g4b.transform.eulerAngles = (new Vector3(0,90,0));
		g4b.transform.Translate(Vector2.down);
		g4b.transform.parent = parentTransform;
	}

	public static Vector3 FindSpaceInBounds(bool avoidPlayer = false, bool extraLarge = false)
	{
		Vector3 playerTransform = cachedPlayer.position;
		Vector3 chosenPos;
		float distFromPlayer = 1.5f;
		if(extraLarge)
			distFromPlayer=4;
		float secondaryAvoidance = 2.3f;
		float distToEdgeOfRect = .5f;
		int tries = 0;
		do{
			if(tries>=3)
				secondaryAvoidance-=.2f;
			float zPos = Random.Range(bottomRect.z+distToEdgeOfRect,topRect.z-distToEdgeOfRect);
			float xPos = Random.Range(leftRect.x+distToEdgeOfRect,rightRect.x-distToEdgeOfRect);
			chosenPos = new Vector3(xPos,0,zPos);
			
			//Debug.Log(Vector3.Distance(playerTransform,chosenPos));
			tries++;
		}while(Vector3.Distance(playerTransform,chosenPos)<distFromPlayer&&(AvoidsLocations(chosenPos, secondaryAvoidance)||extraLarge));
		locationsToAvoid.Add(chosenPos);

		return chosenPos;
	}
	public static bool AvoidsLocations(Vector3 pos, float sa)
	{
		foreach(Vector3 v in locationsToAvoid)
			if(Vector3.Distance(v, pos)<sa)
				return false;
		foreach(DeathBlock d in FindObjectsOfType<DeathBlock>())
			if(Vector3.Distance(d.transform.position, pos) >1)
				return false;

		return true;
	}
	public static void ClearAvoidance()
	{
		locationsToAvoid = new List<Vector3>();
	}
}
