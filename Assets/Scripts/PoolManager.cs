﻿using UnityEngine;
using System.Collections;

public class PoolManager : MonoBehaviour {

	public static Transform poolTransform;
	public GameObject enemyPrefab;
	public GameObject particles;
	public GameObject buzzSaw;
	public static GameObject sParticles;
	public static GameObject sEnemy;
	public static GameObject sBuzzsaw;
	// Use this for initialization
	void Start () {
		Debug.Log("BB");
		poolTransform = this.transform;
		sParticles = particles;
		sEnemy = enemyPrefab;
		sBuzzsaw = buzzSaw;
	}

	public static Transform GetPoolTransform()
	{
		return poolTransform;
	}

	public static void SpawnParticles(Vector3 position)
	{
		//i lied about there being a pool party
		GameObject g = GameObject.Instantiate(sParticles);
		g.transform.position = position;
	}
	public static Squishy GetEnemy()
	{
		if(poolTransform.GetComponentInChildren<Squishy>()!=null){
			Squishy s = poolTransform.GetComponentInChildren<Squishy>();
			s.gameObject.SetActive(true);
			return s;
		}
		else{
			GameObject g = GameObject.Instantiate(sEnemy);
			return g.GetComponentInChildren<Squishy>();
		}
	}
	public static DeathBlock GetSaw()
	{
		if(poolTransform.GetComponentInChildren<DeathBlock>()!=null){
			DeathBlock s = poolTransform.GetComponentInChildren<DeathBlock>();
			s.gameObject.SetActive(true);
			return s;
		}
		else{
			GameObject g = GameObject.Instantiate(sBuzzsaw);
			return g.GetComponent<DeathBlock>();
		}
	}
}
