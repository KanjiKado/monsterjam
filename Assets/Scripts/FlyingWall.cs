﻿using UnityEngine;
using System.Collections;

public class FlyingWall : MonoBehaviour {

	float timeUntilDeath = .5f;
	float speed = 7;
	bool fadeOut = false;
	Vector3 target;
	GameController gameController;
	// Use this for initialization
	void Start () {
		gameController  = FindObjectOfType<GameController>();
		int xPos = (Mathf.Abs(transform.position.x)<1 ? 0 : (transform.position.x>0 ? 1 : -1) );

		if(xPos ==0 )
			xPos = (transform.position.z>0 ? -1: 1) ;

		target = new Vector3(xPos,0,0);
		if(fadeOut)
			target = -target*4;
	}
	
	// Update is called once per frame
	void Update () {

		timeUntilDeath-=Time.deltaTime;
		if(fadeOut)
		{
			timeUntilDeath-=Time.deltaTime;
			GetComponent<Renderer>().material.color = Color.Lerp(Color.white, gameController.GetBackgroundColor(), 1-timeUntilDeath*2);
		}
		transform.Translate(target*speed*Time.deltaTime);
		if(timeUntilDeath<=0)
			Destroy(gameObject);
	}
	public void DisableMotion()
	{
		target = target*-4;
		fadeOut = true;
	}
}
