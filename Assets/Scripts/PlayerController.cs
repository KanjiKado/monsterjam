﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour {

	public GameObject deathParticles;

	private Camera mainCam;
	private LineRenderer line;
	private float lineAlpha = 0;

	private float minLineAlpha = .2f;
	private GameController gameController;

	//used for control
	private bool moving = false;
	private Vector3 targetPosition;
	private Vector3 queuedPosition;
	private float speed = 3f;
	private Rigidbody rigidbody;

	//maybe not transform later but hey it works for now
	private Transform lastWall;
	private Vector3 lastPos;
	private float acceleration = 1;
	private float takeoffAngle;
	private int jumpPower = 0;
	private int maxJumps =2;
	private int currentCombo =0;
	[HideInInspector]
	public string deathReason;
	[HideInInspector]
	private bool dead = false;

	// Use this for initialization
	void Start () {
		mainCam = FindObjectOfType<Camera>();
		Vector3 p = mainCam.ViewportToWorldPoint(new Vector3(transform.position.x,transform.position.z,0));
		transform.position = new Vector3(p.x,transform.position.y,p.z);
		line = GetComponent<LineRenderer>();
		gameController = FindObjectOfType<GameController>();
		rigidbody = GetComponent<Rigidbody>();
	}
	
	public bool isMoving()
	{
		return moving;
	}
	public bool isDead()

	{
		return dead;
	}
	// Update is called once per frame
	void Update () {
		//do we begin to move?
		if(Input.GetMouseButtonDown(0))
			if(jumpPower<maxJumps)
			AimLine();

		//we move
		//fade out line texture
		if(lineAlpha>minLineAlpha){
			lineAlpha-=Time.deltaTime;
			line.material.color = Color.Lerp(gameController.GetBackgroundColor(), gameController.GetForegroundColor(), -.5f+lineAlpha);
		}

		//are we too far off screen?
		if(Mathf.Abs(transform.position.x)>UIScaler.xBound)
			Die ();
		if(transform.position.z>UIScaler.zBoundTop)
			Die ();
		if(transform.position.z<UIScaler.zBoundBot)
			Die ();
	}

	void FixedUpdate()
	{
		
		rigidbody.velocity = Vector3.zero;
		if(moving){
			acceleration+=Time.fixedDeltaTime;
			transform.Translate((transform.position- new Vector3(targetPosition.x+transform.position.x,targetPosition.y+transform.position.y,transform.position.z))*Time.deltaTime*-speed*acceleration*jumpPower);
			lastPos = transform.position;
		}
	}

	
	void OnCollisionEnter(Collision c)
	{
		HandleCollision(c);
	}

	void OnCollisionStay(Collision c)
	{
		HandleCollision(c);
	}
	public void Die(bool delay = false)
	{
		dead = true;
		GameObject g = GameObject.Instantiate(deathParticles);
		g.transform.position = transform.position;
		if(delay){
			gameController.StartCoroutine(WaitForGameOver());
		}
		else{
			deathReason = "flew away";
			(mainCam.gameObject.GetComponent("Grayscale") as MonoBehaviour).enabled = true;
			FindObjectOfType<UIManager>().DisplayDeath();
		}
		gameObject.SetActive(false);

	}

	IEnumerator WaitForGameOver()
	{
		float timeWaited = 0;
		while(timeWaited<.4f)
		{
			timeWaited+=Time.deltaTime;
			yield return null;
		}
		(mainCam.gameObject.GetComponent("Grayscale") as MonoBehaviour).enabled = true;
		FindObjectOfType<UIManager>().DisplayDeath();
	}

	void Respawn()
	{

	}
	//reusability!
	void HandleCollision(Collision c)
	{
		//are we heading towards it?
		if(c.gameObject.CompareTag("Hazard"))
		{	
			deathReason = "got popped";
			Die (true);
		}

		if(moving&&c.gameObject.CompareTag("Enemy"))
		{
			currentCombo++;
			if(currentCombo>2)
				gameController.DisplayCombo(currentCombo);
			c.gameObject.GetComponent<Squishy>().Kill();
		}else

		if(c.gameObject.GetComponent<Wall>()!=null){
			if(c.transform.eulerAngles.y>=90){
				if(lastPos.z-transform.position.z>=0&&c.transform.position.z<0)
					return;
				if(lastPos.z-transform.position.z<=0&&c.transform.position.z>0)
					return;
			}else{
				if(lastPos.x-transform.position.x>=0&&c.transform.position.x<0)
					return;
				if(lastPos.x-transform.position.x<=0&&c.transform.position.x>0)
					return;
			}
			if(moving){
				gameController.ShakeCamera(takeoffAngle,acceleration);
				acceleration = 1;
				lastWall = c.transform;
				moving = false;
				currentCombo = 0;
				line.material.color = gameController.GetBackgroundColor();
				line.SetPosition(1,Vector3.zero);
				lineAlpha = 0;
				c.gameObject.GetComponent<Wall>().Damage(jumpPower);
				jumpPower = 0;
				//ConstrainToScreen();
			}
		}
	}
	/*
	//this is what i get for being lazy
	void ConstrainToScreen()
	{
		Vector3 playerPosition = mainCam.WorldToScreenPoint(transform.position);
		Vector3 n = (new Vector3(Mathf.Clamp(playerPosition.x,Screen.width*UIScaler.xLimit,Screen.width-Screen.width*UIScaler.xLimit),
		            Mathf.Clamp(playerPosition.y,Screen.height*UIScaler.yLimit,Screen.height-Screen.height*UIScaler.yLimit),
		            0));
		Vector3 m = mainCam.ScreenToWorldPoint(n);
		Debug.Log("n"+n+" m:"+m);
		transform.position = new Vector3(m.x,transform.position.y,m.z);
	}*/

	void AimLine()
	{
		jumpPower++;
		moving = true;
		UIManager.started = true;
		lineAlpha = 1;
		Vector3 playerPosition = mainCam.WorldToScreenPoint(transform.position);
		float angle = Mathf.Atan2((Input.mousePosition.y-playerPosition.y), (Input.mousePosition.x-playerPosition.x));
		bool left = Input.mousePosition.x<playerPosition.x;
		bool up =   Input.mousePosition.y<playerPosition.y;
		
		angle = SmartcastAngle(angle);

		Vector3 m = new Vector3(Mathf.Cos(angle)*9,Mathf.Sin(angle)*9,0);


		targetPosition = new Vector3(m.x, m.y, 0);
		//-transform.position.x
		//	-transform.position.z

		line.SetPosition(1,targetPosition);

		//"smart cast" to avoid hitting any wall we're stuck to.

	}

	void QueueNextLine()
	{

	}

	float SmartcastAngle (float angle)
	{
		if(lastWall!=null)
		{
			if(lastWall.eulerAngles.y!=0)//then it's a rotated wall
			{
				if(angle*Mathf.Rad2Deg <180&&angle*Mathf.Rad2Deg>150)
				{
					angle = 180*Mathf.Deg2Rad;
				} else if
					
				(angle*Mathf.Rad2Deg <30&&angle*Mathf.Rad2Deg>0)
				{
					angle = 0f*Mathf.Deg2Rad;
				}
				/*
				Debug.Log("turn");
				float up = transform.position.y > lastWall.position.y ? .5f : -.5f;
				float sinVal = Mathf.Sin(targetPos.x/transform.position.x);
				
				Debug.Log(sinVal+","+up);
				//we want to detect between 0 and .5 or 0 and -.5
				float bigNum = Mathf.Max(0,up);
				float smallNum = Mathf.Min(0,up);
				if(sinVal >= bigNum && sinVal <= smallNum){
					targetPos.x = transform.position.x;
					Debug.LogWarning("constrained0");
				}*/
			} else {
				
				if((angle*Mathf.Rad2Deg <90&&angle*Mathf.Rad2Deg>60))
				{
					angle = 90f*Mathf.Deg2Rad;
				} else if
					
					((Mathf.Rad2Deg>270)&&angle*Mathf.Rad2Deg<300)
				{
					angle = 270f*Mathf.Deg2Rad;
				}
			}
		}
		takeoffAngle = angle;
		return angle;
	}
}
