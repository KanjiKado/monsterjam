﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public Material backgroundColor;
	public Material foregroundColor;
	public GameObject camObject;
	public Camera camera;
	public GameObject wallFrag;
	public float shakeRemaining = 0;
	float shakeDivisor = 6;
	float amountToSubtract = 0;
	float flashAmt = 0;
	Vector3 targetPosition;
	UIManager ui;
	void Start()
	{
		ui = FindObjectOfType<UIManager>();
		camera = camObject.GetComponent<Camera>();
	}

	// Update is called once per frame
	void Update () {
		if(shakeRemaining>0)
		{

			amountToSubtract+=Time.deltaTime;
			shakeRemaining = shakeRemaining/2;
			shakeRemaining-=Time.deltaTime;
			shakeRemaining = Mathf.Max(shakeRemaining,0);

			camObject.transform.position = new Vector3(targetPosition.x*shakeRemaining+Random.Range(shakeRemaining,-shakeRemaining)/shakeDivisor,
			                                           targetPosition.y,
			                                           targetPosition.z*shakeRemaining+Random.Range(shakeRemaining,-shakeRemaining)/shakeDivisor);

			targetPosition = Vector3.Lerp(targetPosition,Vector3.up,amountToSubtract*.01f);

			if(shakeRemaining<=0)
				camObject.transform.position = new Vector3(0,camObject.transform.position.y, 0);
		}
		if(flashAmt>0)
		{
			camera.backgroundColor = Color.Lerp(backgroundColor.color, Color.white, flashAmt);
			flashAmt-=Time.deltaTime*.2f;
		}
	}

	public void DisplayCombo(int amt)
	{
		ui.DisplayCombo(amt);
	}

	public GameObject GetFragReference()
	{
		return wallFrag;
	}

	public Color GetBackgroundColor()
	{
		return backgroundColor.color;
	}
	public Color GetForegroundColor()
	{
		return foregroundColor.color;
	}
	public void ShakeCamera(float angle, float intensity)
	{
		amountToSubtract = 0;
		shakeRemaining = intensity;
		float m = .25f;
		targetPosition = (new Vector3(m*Mathf.Cos(angle),1,m*Mathf.Sin(angle)));
		camObject.transform.position = targetPosition;
	}
	public void HandleSlam()
	{
		flashAmt = .2f;
		shakeRemaining*=8;
	}
}
